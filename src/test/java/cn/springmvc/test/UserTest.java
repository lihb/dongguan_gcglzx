package cn.springmvc.test;

import cn.springmvc.entity.User;
import cn.springmvc.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;
import java.util.List;


public class UserTest {

private UserService userService;
	
	@Before
	public void before(){                                                                    
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:conf/spring.xml"
				,"classpath:conf/spring-mybatis.xml");
		userService = (UserService) context.getBean("userServiceImpl");
	}
	
	@Test
	public void addUser(){
		User user = new User();
		user.setNickname("小5");
		user.setUsername("xxxx");
		user.setRole(1);
		user.setEmail("xiaosi@gmail.com");
		user.setAvatar("http://xx.yy.xom");
		user.setCreateTime(new Date());
		user.setIntro("自我介紹3");
		user.setPassword("password");
		System.out.println(userService.insertUser(user));
	}

	@Test
	public void queryUserById(){
		User user = userService.queryUserById(3);
		if (null != user) {
			System.out.println(user.toString());
		}else {
			System.out.println("没有该用户");
		}
	}

	@Test
	public void queryAllUser(){
		List<User> users = userService.queryAllUser();
		if (null != users) {
			for (User user : users) {
				System.out.println(user.toString());
			}
		}else {
			System.out.println("没有该用户");
		}
	}
}
