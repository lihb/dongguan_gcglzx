package cn.springmvc.test;

import cn.springmvc.entity.UserCourse;
import cn.springmvc.service.UserCourseService;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;


public class UserCourseTest {

	private UserCourseService userCourseService;

	private UserCourse userCourse;


	private static final Logger logger = Logger.getLogger("UserCourseTest");
	
	@Before
	public void before(){                                                                    
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:conf/spring.xml"
				,"classpath:conf/spring-mybatis.xml");
		userCourseService = (UserCourseService) context.getBean("userCourseServiceImpl");
		userCourse = (UserCourse) context.getBean("userCourse");
	}

	@Test
	public void testGetUserCourseByUserId() {
		List<UserCourse> userCourseList = userCourseService.getUserCourseByUserId(1);

		for (int i = 0; i < userCourseList.size(); i++) {
			UserCourse userCourse = userCourseList.get(i);
			System.out.println(userCourse.toString());
		}
	}

	@Test
	public void testGetUserCourseByCourseId() {
		List<UserCourse> userCourseList = userCourseService.getUserCourseByCourseId(1);

		for (int i = 0; i < userCourseList.size(); i++) {
			UserCourse userCourse = userCourseList.get(i);
			System.out.println(userCourse.toString());
		}
	}

	@Test
	public void insertUserCourseRecord() {
		userCourse.setCid(1);
		userCourse.setStatus(2);
		userCourse.setUid(4);
		System.out.println(userCourseService.insert(userCourse));
	}

}
