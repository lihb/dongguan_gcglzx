package cn.springmvc.test;

import cn.springmvc.entity.Question;
import cn.springmvc.service.CourseService;
import cn.springmvc.service.QuestionService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by Administrator on 2017/5/30.
 */
public class QuestionTest {

    private QuestionService questionService;

    @Before
    public void before(){
        @SuppressWarnings("resource")
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:conf/spring.xml"
                ,"classpath:conf/spring-mybatis.xml");
        questionService = (QuestionService) context.getBean("questionServiceImpl");
    }


    @Test
    public void queryQuestionByType(){
        List<Question> questions = questionService.selectData(1);
        if (null != questions) {
            for (Question question : questions) {
                System.out.println("---" + question.toString());
            }
        }else {
            System.out.println("没有该题目");
        }
    }
}
