package cn.springmvc.test;

import cn.springmvc.entity.Course;
import cn.springmvc.service.CourseService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;


public class CourseTest {

private CourseService courseService;
	
	@Before
	public void before(){                                                                    
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:conf/spring.xml"
				,"classpath:conf/spring-mybatis.xml");
		courseService = (CourseService) context.getBean("courseServiceImpl");
	}
	
	@Test
	public void addCourse(){
//		Course course = new Course();
//		course.setCourseId(1);
//		course.setCourseAttachment("http://attach.b.com");
//		course.setCourseCover("http://cover.b.com");
//		course.setCourseCredit(5);
//		course.setCourseDescription("课程描述");
//		course.setCourseIsElective(false);
//		course.setCourseScorePass(60);
//		course.setCourseTag("移动");
//		course.setCourseTime(45);
//		course.setCourseType("工程");
//		course.setCourseWareId(1);
//		System.out.println(courseService.insertCourse(course));
	}

	@Test
	public void queryCourseByWareId() {

		List<Course> courseList = courseService.queryCourseByWareId(1);
		for (Course course : courseList) {
			System.out.println(course);
		}
	}


}
