<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<head>
    <%
        String path = request.getContextPath();
    %>
    <title>
        注册新用户
    </title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="Cache-Control" content="no-transform">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/NewDefault.css">
    <script src="/js/zhezhao.js?v=1" type="text/javascript"></script>
    <style type="text/css">
        html,
        body {
            height: 100%;
            padding: 0;
            margin: 0;
        }
    </style>
    <style type="text/css">
        .fancybox-margin {
            margin-right: 15px;
        }
    </style>
</head>

<body>
<style>
    #BS {
        margin: 0 auto -250px;
        padding: 0;
        width: 100%;
        min-height: 100%;
        height: auto !important;
        height: 100%;
        min-width: 980px;
    }
</style>

<div id="BS">
    <div class="wheader">
        <div class="wheader-inner">
            <div class="wheader-lbox">
                <div class="wheader-sojumplogo">
                    <h1>监理工程师在线考试</h1>
                </div>
            </div>
            <div class="wheader-menu">
                <div class="wheader-menu-item">
                    <ul>
                        <li id="ctl00_liIndex" class="acitve">
                            <a title="监理工程师在线课程首页" href="index.do">首&nbsp;&nbsp;页</a>
                        </li>
                        <li>
                            <a title="课件中心" href="toCourseware.do">课件中心</a>
                        </li>
                        <li>
                            <a title="在线考试" href="toTiku.do">在线考试</a>
                        </li>
                        <li>
                            <a title="个人管理中心" href="toUser.do">个人管理中心</a>
                        </li>
                    </ul>
                </div>
                <div class="wheader-reg">
                    <ul>
                        <li id="ctl00_liRegister"><a href="toRegister.do"><span class="btn btn-s site-res">注册</span></a>
                        </li>
                        <li id="ctl00_liLogin"><a href="toLogin.do"><span class="btn btn-s site-login">登录</span></a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div style="margin:0 auto;  width:945px; text-align:left; margin-top:30px;;">

        <style>
            .wheader {
                background: #f6f6f6;
            }
        </style>
        <div>

            <div id="mp4" class="mp4">
                <h3><i class="fa fa-video-camera"></i>视频介绍</h3>
                <video controls="">
                    <source src="<%=path%>/video/gkzy.webm" type="video/webm">
                    <source src="<%=path%>/video/gkzy.mp4" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>
        </div>

    </div>
    <div style="height:280px;"></div>
</div>
<div id="Wfooter" style="height:320px;">
    <div id="Wcontact">
        <ul class="W-ulCons" style="border:0; padding:0; width:206px">
            <img src="<%=path%>/images/sojumpwjxlogonew.png" width="121" style="display:block;"/>
        </ul>
        <ul class="W-ulCons">
            <li class="i-Common WulCons-i1">
                <a href="index.do">首页</a></li>

        </ul>
        <ul class="W-ulCons">
            <li class="i-Common WulCons-i2">
                <a href="toCourseware.do">课件中心</a></li>


        </ul>
        <ul class="W-ulCons">
            <li class="i-Common WulCons-i3">
                <a href="toTiku.do">在线考试</a></li>

        </ul>
        <ul class="W-ulCons" style="width:165px; float:right;">
            <li class="i-Common WulCons-i4"><a href="toUser.do">个人中心</a></li>
        </ul>
    </div>
    <div class="W-coypright">
        <ul class="WalCons W-fttx">
            <li><span class="W-Rttx">Contact US</span><span class="W-Rttx">Tel：400-839-8080</span><span class="W-Rttx">E-mail：cs@sojump.com</span>
            </li>
        </ul>

        <ul class="WalCons Walcons_txt">
            <li class="W-mark">
                <img width="210" height="25" alt="" src="//image.sojump.com/images/MasterNew/bottom_right.gif"><br>
                监理工程师在线课程
            </li>
        </ul>
    </div>
    <div id="ctl00_divImage" class="W-Fimg" align="center">
        <div style="position:relative">
            <a href="/images/html/promotemsg/licence_pic.jpg" target="_blank"
               style="background:url('//image.sojump.com/images/outside.gif') no-repeat; width:111px; height:42px; display:inline-block; border:0;"></a>
            &nbsp;&nbsp;&nbsp;
            <a href="http://sh.cyberpolice.cn" target="_blank"
               style="background:url('//image.sojump.com/images/MasterNew/police.gif') no-repeat; width:102px; height:35px; display:inline-block; border:0;"></a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="http://www.zx110.org" target="_blank"
               style="background:url('//image.sojump.com/images/MasterNew/socily.gif') no-repeat; width:102px; height:35px;border:0;display:inline-block; "></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="http://www.miibeian.gov.cn/" target="_blank"
               style="background:url('//image.sojump.com/images/MasterNew/icp.gif') no-repeat; width:102px; height:35px;border:0;display:inline-block; "></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a key="549255863b05a3da0fbc4cf9" logo_size="83x30" logo_type="realname" href="http://www.anquan.org">
                <script src="//static.anquan.org/static/outer/js/aq_auth.js"></script>
            </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a key="553dfded58725379d18afc34" logo_size="83x30" logo_type="business" href="http://www.anquan.org">
                <script src="//static.anquan.org/static/outer/js/aq_auth.js"></script>
            </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <div style="display:none;">
        </div>
    </div>
</div>
</body>
<div></div>