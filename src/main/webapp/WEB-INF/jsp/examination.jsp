<%@ page import="cn.springmvc.entity.Question" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

	<head id="ctl00_Head1">
		<title>
			监理工程师试卷
		</title>
		<%
			String path = request.getContextPath();
			List<Question> questionList = (List<Question>) request.getAttribute("questionList");
			int size = questionList.size();

		%>
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<link id="ctl00_linkDefault" type="text/css" rel="stylesheet" href="<%=path%>/css/NewDefault.css">
		<link href="<%=path%>/css/q.css" rel="stylesheet" type="text/css">
		<link href="<%=path%>/css/newsolid_38.css" rel="stylesheet" type="text/css">
		<script src="<%=path%>/js/jquery-3.2.1.js"></script>
	</head>
	<script type="text/javascript">
        $(document).ready(function () {
            function getRadioValue(radioName) {
                var chkRadio = document.getElementsByName(radioName);
                for (var i = 0; i < chkRadio.length; i++) {
                    if (chkRadio[i].checked){
                        return chkRadio[i].value;
                    }
                }
            }

            function loadData() {
				$.getJSON('<%=path%>/question/getDataByType.do', function(data){

                    $.each(data.list, function(i, item) {
                        var chk = getRadioValue("q"+i);
                        alert(chk);
                        if(chk == item.answer) {
                            alert("yes");
                        }else {
                            alert("no");
						}
                    });
				});
            }
            // 获取选择的答案，上传到后台获取分数
            $("#submit_button").click(function () {
                var choices =new Array();
                for (var i = 0; i < <%=size%>; i++) {
                    var chkValue = getRadioValue("q"+(i+1));
                    if(typeof(chkValue) == "undefined"){
                        chkValue = "";
					}
                    choices[i] = chkValue;
				}
                $.ajax({
					url:'<%=path%>/question/computerScore.do',
					data:{'choices':choices},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        alert("得分:"+data.list);
                    }
                });
            });
        });

	</script>

	<body style="">

		<div id="divNotRun" style="height:100px; text-align:center; display:none;"></div>
		<div id="jqContent" class="" style="text-align: left; ">
			<div id="headerCss" style="overflow-x: hidden; overflow-y: hidden; ">
				<div id="ctl00_header">
				</div>
			</div>
			<div id="mainCss">
				<div id="mainInner">
					<div id="box">
						<div class="survey" style="margin:0px auto;">

							<div id="ctl00_ContentPlaceHolder1_JQ1_divHead" class="surveyhead" style="border: 0px;">
								<h1 id="ctl00_ContentPlaceHolder1_JQ1_h1Name" style="position:relative;">
        <span id="ctl00_ContentPlaceHolder1_JQ1_lblQuestionnaireName">监理工程师试卷</span><span id="ctl00_ContentPlaceHolder1_JQ1_lblMobile" style="position:absolute; right:-10px; top:6px;"><a href="javascript:;" onclick="showData(event,this,&quot;https://sojump.com/m/4383415.aspx?tpd=1&amp;p=1&quot;,0);" style="display:inline-block;width:50px;height:50px;background:url(&quot;//down.sojump.com/handler/qrcode.ashx?chs=50x50&amp;chl=https%3a%2f%2fsojump.com%2fm%2f4383415.aspx%3ftpd%3d1%26p%3d1&quot;) no-repeat;"></a></span>

     </h1>
								<div id="ctl00_ContentPlaceHolder1_JQ1_divMob">
									<div id="divMa" style="display:none; position:absolute; z-index:100;border:1px solid #DBDBDB;width:200px; height:220px; background:white;" onclick="showData(event);">
										<div style="text-align:center;">
											<a href="javascript:" style="border:none;"><img alt="" width="200" height="200"></a>
											<div>手机扫描二维码答题</div>
										</div>
									</div>

								</div>

								<div style="clear: both;">
								</div>
								<script type="text/javascript">
									var hasQJump = 1;
								</script>

								<div id="ctl00_ContentPlaceHolder1_JQ1_divJump">
									<div id="divS" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; position: absolute; z-index: 30000; width: 1250px; height: 3000px; left: 0px; top: 0px; opacity: 0.61; display: none;">
									</div>
									<div id="loadbox" style="border: 2px solid rgb(31, 73, 125); position: absolute; top: 300px; left: 460px; font-size: 12px; width: 400px; height: 100px; background-color: rgb(79, 129, 189); text-align: center; color: rgb(255, 255, 255); z-index: 50000; display: none;">
										<div style="height: 30px; padding: 2px; text-align: left;">问卷正在加载中，请稍候...</div>
										<img src="/GreyBox/ajax-loading.gif" width="160" height="20">
										<div style="color: Red; margin: 5px 0; font-size:14px;">如果由于网络原因导致此框一直不消失，请重新刷新页面！</div>
									</div>
								</div>

								<div style="clear: both;">
								</div>

							</div>

							<div id="ctl00_ContentPlaceHolder1_JQ1_question" class="surveycontent">
								<div id="divMaxTime" style="text-align: center; display: none; width:80px; background:white; position:fixed;top:135px;border:1px solid #dbdbdb; padding:8px;z-index:10;">
									<div id="spanTimeTip" style="border-bottom:1px solid #dbdbdb;height:30px; line-height:30px;">本页时间剩余</div>
									<div style="color: Red;font-size:16px; height:30px; line-height:30px;" id="spanMaxTime"></div>
								</div>
								<div id="ctl00_ContentPlaceHolder1_JQ1_surveyContent">
									<fieldset class="fieldset" id="fieldset1">
										<div class="div_title_page_question"><span style="font-size:32px;color:#d40a00;">监理工程师小测</span></div>
										<%
											for (int i = 0; i <size; i++) {
												Question question = questionList.get(i);
										%>
										<div class="div_question" id="div<%=i+1%>">
											<div class="div_title_question_all">
												<div class="div_topic_question"><%= i+1 %>.</div>
												<div id="divTitle<%=i+1%>" class="div_title_question"><%=question.getSubject()%>&nbsp;(&nbsp;)。<span class="req">&nbsp;*</span></div>
												<div style="clear:both;"></div>
											</div>
											<div class="div_table_radio_question" id="divquestion<%= i+1 %>">
												<div class="div_table_clear_top"></div>
												<ul class="ulradiocheck">
													<li style="width: 99%;">
														<a href="javascript:" class="jqRadio" rel="q<%= i+1 %>_1"></a><input  name="q<%= i+1 %>" id="q<%= i+1 %>_1" value="<%=question.getA()%>" type="radio"><label for="q<%= i+1 %>_1"><%=question.getA()%></label></li>
													<li style="width: 99%;">
														<a href="javascript:" class="jqRadio" rel="q<%= i+1 %>_2"></a><input  name="q<%= i+1 %>" id="q<%= i+1 %>_2" value="<%=question.getB()%>" type="radio"><label for="q<%= i+1 %>_2"><%=question.getB()%></label></li>
													<li style="width:99%;">
														<a href="javascript:" class="jqRadio" rel="q<%= i+1 %>_3"></a><input  name="q<%= i+1 %>" id="q<%= i+1 %>_3" value="<%=question.getC()%>" type="radio"><label for="q<%= i+1 %>_3"><%=question.getC()%></label></li>
													<li style="width:99%;">
														<a href="javascript:" class="jqRadio" rel="q<%= i+1 %>_4"></a><input  name="q<%= i+1 %>" id="q<%= i+1 %>_4" value="<%=question.getD()%>" type="radio"><label for="q<%= i+1 %>_4"><%=question.getD()%></label></li>
													<div style="clear:both;"></div>
												</ul>
												<div style="clear:both;"></div>
												<div class="div_table_clear_bottom"></div>
											</div>
											<div class="errorMessage"></div>
										</div>
										<%
											}
										%>

									</fieldset>
								</div>
								<div class="shopcart" id="shopcart" >
								</div>
								<div style="padding-top: 6px;clear:both; padding-bottom:10px;" id="submit_div">
									<table id="submit_table" style="margin: 20px auto;">

										<tbody>
											<tr>
												<td id="ctl00_ContentPlaceHolder1_JQ1_tdCode" style="display: none;">
													<input id="yucinput" size="14" maxlength="10" onkeydown="enter_clicksub(event);" name="yucinput" class="inputtext" style="height:24px;line-height:24px;" type="text">&nbsp;&nbsp;<img id="imgCode" alt="验证码" title="看不清吗？点击可以刷新" style="vertical-align: bottom; cursor:pointer; display:none;">
												</td>

												<td>
													<div id="divCaptcha" >
														<img alt="验证码" title="看不清吗？点击可以刷新" captchaid="" instanceid="">
													</div>
												</td>
												<td>
													<input class="submitbutton" value="提交"  onmouseout="this.className='submitbutton';" id="submit_button" style="padding:0 24px;height:32px;" type="button">

												</td>

												<td>

												</td>

												<td style="position:relative;" align="right">
													<span id="spanTest" style="display:none; position:absolute; width:120px;top:5px;"><input class="finish cancle" value="试填问卷" id="submittest_button" title="只有发布者才能看到试填按钮，试填的答卷不会参与结果统计！" type="button"><a title="只有发布者才能看到试填按钮，试填的答卷不会参与结果统计！" onclick="alert(this.title);" style="color: green" href="javascript:void(0);"><b>(?)</b></a></span>
												</td>

												<td align="right" valign="bottom">
												</td>

											</tr>
										</tbody>
									</table>
									<div style="clear:both;"></div>

								</div>
								<div id="submit_tip" style="display: none; background-color: #f04810; color: White;
        margin-bottom: 20px; padding: 10px">
							</div>
							<div id="divMatrixRel" style="position: absolute; display: none; width:300px; margin: 0 10%;">
								<input id="matrixinput" style="width: 100%;height:24px;line-height:24px;color:#8c8c8c;" class="inputtext" type="text">
							</div>
							<div  id="divNA">
								<input value="1" name="divNA" id="divNA_1" type="radio"><label for="divNA_1">A.男</label><input value="2" name="divNA" id="divNA_2" type="radio"><label for="divNA_1">B.女</label>
							</div>
						</div>

						<div id="ctl00_ContentPlaceHolder1_JQ1_divLeftBar" style="text-align: center; position: absolute; width: 50px; padding: 8px 0px; left: 1085px; top: 539px;" class="leftbar">
							<div id="divProgressBar">
								<div style="text-align:left;">
									<span id="loadprogress" style="font-weight: bold;visibility: hidden;">&nbsp;&nbsp;0%</span>
								</div>
								<div id="ctl00_ContentPlaceHolder1_JQ1_divProgressImg" style="float: left; padding-left:15px;visibility: hidden;">
									<div id="loading">
										<span id="loadcss" style="height: 0%;line-height:0;font-size:0; overflow:hidden;"></span>
									</div>
								</div>
								<div style="float: left; width:14px; line-height:0;" id="divSaveText">

								</div>
								<div class="divclear"></div>
							</div>
							<div style="float: left;padding-left: 2px; visibility:hidden;">

							</div>

							<div style="clear: both;">
							</div>
						</div>

						<div style="clear: both;">
						</div>

						<div id="divDescPop" >
							<div style="padding:10px;  overflow:auto;line-height:20px;font-size:14px;" id="divDescPopData"></div>
						</div>
						<div id="rbbox" style="display:none; height:70px;">

						</div>
						<div style="clear: both;">
						</div>
					</div>

					<div style="margin:30px auto 0; padding-top:30px; overflow: hidden; width:100%;">
						<div style="border-top: 1px solid #bbbbbb; font-size: 0; height: 1px; line-height: 1px;
                            width: 98%; margin: 0 auto;">
					</div>
					<table cellspacing="0" cellpadding="0" width="100%" border="0">
						<tbody>
							<tr>
								<td height="10px">
								</td>
							</tr>
							<tr>
								<td align="center" valign="middle">
									<table cellspacing="0" cellpadding="0" width="100%" border="0">
										<tbody>
											<tr id="ctl00_trCopy">
												<td style="font-size: 12px; font-family: Tahoma, 宋体; color: #666666;" align="center">

												</td>
											</tr>

											<tr id="ctl00_trPoweredBy">
												<td style="color: #666666; font-family: Tahoma, 宋体;" align="center">
													<div style="height: 10px;">
													</div>
													<div id="divBannerLogo"><span id="ctl00_lblPowerby"><a href="http://www.sojump.com/" target="_blank" class="link-666" title="问卷星-专业的在线问卷调查、测评、投票平台">Powered By <span style="color:#30A6E2;">问卷星</span></a>
														</span>
													</div>

												</td>
											</tr>

											<tr>
												<td>

												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div style="clear: both;">
		</div>
		</div>
		<div id="footercss">
			<div id="footerLeft">
			</div>
			<div id="footerCenter">
			</div>
			<div id="footerRight">
			</div>
		</div>
		<div style="clear: both; height: 10px;">
		</div>
		<div style="height: 20px;">
			&nbsp;</div>
		</div>

		<div style="clear:both;"></div>



	</body>

</html>