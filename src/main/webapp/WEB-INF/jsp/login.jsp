<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>
        监理工程师
    </title>
    <%
        String path = request.getContextPath();
        System.out.println("======login=====path = " + path);
    %>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="Cache-Control" content="no-transform">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/NewDefault.css">
    <style type="text/css">
        html,
        body {
            height: 100%;
            padding: 0;
            margin: 0;
        }
    </style>
    <style type="text/css">
        .fancybox-margin {
            margin-right: 15px;
        }
    </style>
</head>

<body>
<style>
    #BS {
        margin: 0 auto -250px;
        padding: 0;
        width: 100%;
        min-height: 100%;
        height: auto !important;
        height: 100%;
        min-width: 980px;
    }
</style>

<div id="BS">
    <div class="wheader">
        <div class="wheader-inner">
            <div class="wheader-lbox">
                <div class="wheader-sojumplogo">
                    <h1>问卷调查 • 考试 • 投票</h1>
                </div>
            </div>
            <div class="wheader-menu">
                <div class="wheader-menu-item">
                    <ul>
                        <li id="ctl00_liIndex" class="acitve">
                            <a title="监理工程师在线课程首页" href="index.do">首&nbsp;&nbsp;页</a>
                        </li>
                        <li>
                            <a title="课件视频" href="toCourseware.do">课件中心</a>
                        </li>
                        <li>
                            <a title="课件视频" href="toTiku.do">在线考试</a>
                        </li>
                        <li>
                            <a title="课件视频" href="toUser.do">个人管理中心</a>
                        </li>
                    </ul>
                </div>
                <div class="wheader-reg">
                    <ul>
                        <li id="ctl00_liRegister">
                            <a href="toRegister.do"><span class="btn btn-s site-res">注册</span></a>
                        </li>
                        <li id="ctl00_liLogin">
                            <a href="toLogin.do"><span class="btn btn-s site-login">登录</span></a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div style="margin:0 auto;  width:945px; text-align:left; margin-top:30px;;">

        <style>
            .wheader {
                background: #f6f6f6;
            }
        </style>
        <form name="aspnetForm" method="post" action="${pageContext.request.contextPath}/user/login.do" id="aspnetForm">
            <div>
                <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE"
                       value="/wEPDwUKMTk3NTU0NDU3NGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFJGN0bDAwJENvbnRlbnRQbGFjZUhvbGRlcjEkUmVtZW1iZXJNZQ==">
            </div>

            <div>

                <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION"
                       value="/wEdAAYoM12aVmHD2ECpMMA0syBJO6eeGTgad8ksqhDrfRl8wTkBDO5iLfQzicpFe97ObAwXKJ4BaIwAVqp1tKKAEy0u9OTkg+QdHm5OmdKOuPOcGpV4bqusu2zQztdPuWZ1xvo=">
            </div>
            <div id="MainDiv">
                <div id="ctl00_ContentPlaceHolder1_divSojump" style="padding-top:10px;">
                    <link href="<%=path%>/css/nlogin2.css" type="text/css" rel="stylesheet">
                    <div id="LoginDiv">
                        <div class="login_lbg"></div>

                        <div id="LoginBox">
                            <div id="LoginBox-title">用户登录</div>
                            <div id="LoginBox-con">
                                <ul>
                                    <li class="liuser">
                                        <span style="float:left;height:30px;line-height:30px; margin-right:3px;width:44px;">用 户 名</span><i></i><input
                                            name="username" type="text" id="ctl00_ContentPlaceHolder1_UserName"
                                            class="username_input" onfocus="if(value=='用户名/Email/手机'){value='';}"
                                            onblur="if(value==''){value='用户名/Email/手机'}"><input type="hidden"
                                                                                                name="ctl00$ContentPlaceHolder1$hfUserName"
                                                                                                id="ctl00_ContentPlaceHolder1_hfUserName">
                                        <div class="divclear"></div>
                                    </li>
                                    <li class="lipwd"><span
                                            style="float:left;height:30px;line-height:30px; margin-right:3px;width:44px;">密&nbsp;&nbsp;&nbsp;&nbsp;码</span><i></i><input
                                            name="password" type="password" id="ctl00_ContentPlaceHolder1_Password"
                                            class="passward_input">
                                        <div class="divclear"></div>
                                    </li>

                                    <li id="ctl00_ContentPlaceHolder1_trRemember" style="padding-left:45px;">
												<span>
                                <span class="login_checkbox centertxt"><input id="ctl00_ContentPlaceHolder1_RememberMe"
                                                                              type="checkbox"
                                                                              name="ctl00$ContentPlaceHolder1$RememberMe"></span>自动登录</span>
                                        <span style="display:inline-block; margin-left:40px;"><a
                                                id="ctl00_ContentPlaceHolder1_PasswordRecoveryLink"
                                                href="#">忘记用户名/密码？</a></span>
                                    </li>
                                    <li class="centertxt" style="padding-left:45px;">
                                        <div style="float:left;padding-right:50px;"><input type="submit"
                                                                                           name="ctl00$ContentPlaceHolder1$LoginButton"
                                                                                           value="登 录"
                                                                                           class="submitbutton"
                                                                                           onmouseover="this.className='submitbutton submitbutton_hover';"
                                                                                           onmouseout="this.className='submitbutton';"
                                                                                           style="padding:0 25px;">
                                        </div>
                                        <div style="float:left;">
                                            <a href="#"
                                               style="background:url('<%=path%>/images/wjx/ico_share/qqlogin.gif'); display:inline-block; width:76px; height:26px;"></a>
                                        </div>
                                        <div class="divclear"></div>
                                    </li>

                                </ul>

                            </div>
                            <div id="LoginBox-bottom" style="text-align:center;">
                                <a href="toRegister.do" id="ctl00_ContentPlaceHolder1_hrefRegister"
                                   style="color:#fff; display:inline-block;width:344px; margin-top:8px; font-size:16px;"
                                   title="只需２０秒，就可完成注册">立即注册</a>
                            </div>
                            <div style="clear:both;"></div>
                        </div>

                    </div>

                </div>
            </div>

        </form>

    </div>
    <div style="height:280px;"></div>
</div>
<div id="Wfooter" style="height:320px;">
    <div id="Wcontact">
        <ul class="W-ulCons" style="border:0; padding:0; width:206px">
            <img src="<%=path%>/images/sojumpwjxlogonew.png" width="121" style="display:block;"/>
        </ul>
        <ul class="W-ulCons">
            <li class="i-Common WulCons-i1">
                <a href="index.do">首页</a></li>

        </ul>
        <ul class="W-ulCons">
            <li class="i-Common WulCons-i2">
                <a href="toCourseware.do">课件中心</a></li>


        </ul>
        <ul class="W-ulCons">
            <li class="i-Common WulCons-i3">
                <a href="toTiku.do">在线考试</a></li>

        </ul>
        <ul class="W-ulCons" style="width:165px; float:right;">
            <li class="i-Common WulCons-i4"><a href="toUser.do">个人中心</a></li>
        </ul>
    </div>
    <div class="W-coypright">
        <ul class="WalCons W-fttx">
            <li><span class="W-Rttx">Contact US</span><span class="W-Rttx">Tel：400-839-8080</span><span class="W-Rttx">E-mail：cs@sojump.com</span>
            </li>
        </ul>

        <ul class="WalCons Walcons_txt">
            <li class="W-mark">
                <img width="210" height="25" alt="" src="//image.sojump.com/images/MasterNew/bottom_right.gif"><br>
                问卷调查•考试•投票
            </li>
        </ul>
    </div>

</div>



</body>
<div></div>
</html>