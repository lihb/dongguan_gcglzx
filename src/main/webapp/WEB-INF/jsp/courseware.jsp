<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="">
<head><title>
    公开问卷页面
</title>
    <%
        String path = request.getContextPath();
    %>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="Cache-Control" content="no-transform">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/NewDefault.css">
    <style type="text/css">
        html, body {
            height: 100%;
            padding: 0;
            margin: 0;
        }

    </style>
    <style type="text/css">.fancybox-margin {
        margin-right: 15px;
    }</style>
</head>
<body>
<style>
    #BS {
        margin: 0 auto -250px;
        padding: 0;
        width: 100%;
        min-height: 100%;
        height: auto !important;
        height: 100%;
        min-width: 980px;
    }
</style>

<div id="BS">
    <div class="wheader">
        <div class="wheader-inner">
            <div class="wheader-lbox">
                <div class="wheader-sojumplogo">
                    <h1>监理工程师在线考试</h1>
                </div>
            </div>
            <div class="wheader-menu">
                <div class="wheader-menu-item">
                    <ul>
                        <li id="ctl00_liIndex"><a href="index.do" title="监理工程师在线课程首页">首&nbsp;&nbsp;页</a></li>
                        <li class="acitve"><a title="课件视频" href="toCourseware.do">课件中心</a></li>
                        <li><a title="课件视频" href="toTiku.do">在线考试</a></li>
                        <li><a title="课件视频" href="toUser.do">个人管理中心</a></li>
                    </ul>
                </div>
                <div class="wheader-reg">
                    <ul>
                        <li id="ctl00_liRegister"><a href="toRegister.do"><span class="btn btn-s site-res">注册</span></a></li>
                        <li id="ctl00_liLogin"><a href="toLogin.do"><span class="btn btn-s site-login">登录</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div style="margin:0 auto;  text-align:left;;">

        <link rel="stylesheet" href="<%=path%>/css/pubsurvey.css">
        <div class="surveybanner">
            <ul>
                <li>
                    <form class="search-form">
                        <input type="text" id="txtInput" placeholder="输入关键词，在13,114,671份问卷中搜索" class="survey-search"
                               onkeypress="return enterclick2(event,'txtInput');">
                        <a href="###" class="btn surveysbtn" onclick="search_click('txtInput');"><i
                                class="surver-searchicon"></i>搜索</a>
                    </form>
                </li>
                <li><a title="创建新问卷" class="btn createbtn" href="toCreateblank.do">
                    <img src="<%=path%>/images/jiahao.png">创建新问卷</a></li>
                <div class="clearfix"></div>
            </ul>
        </div>
        <div id="ctl00_ContentPlaceHolder1_divSurveyList" class="survey-container">
            <div class="survey-item">
                <!--<ul id="TagList" class="survey-item-title margin_center">
                    <li class="selectTag"><a onclick="selectTag('tagContent0',this)" id="hrefdiaocha" href="#diaocha"><i class="sitem01"></i><em></em><span>视频培训</span></a></li>
                    <li><a onclick="selectTag('tagContent1',this)" id="hrefks" href="#ks"><i class="sitem02"></i><em></em><span>在线考试</span></a> </li>




                </ul>-->
                <div id="tagContent" class="survey-ibox">

                    <!--视频中心-->
                    <div id="tagContent0" class="survey-ibox-ul selectTag">
                        <ul class="survey-item-listbox">
                            <li><a class="video_wrap" href="toVideo.do">
                                <img src="<%=path%>/images/sitem01_01.jpg">
                            </a>
                                <a href="examination.do">
                                    <div class="zxks survey-item-txtbox">
                                        <h2 class="survey-item-txtname">高空作业视频</h2>
                                        <div class="clearfix"></div>
                                        <span class="txtboxspanL">45 道题</span> <span class="txtboxspanR">在线考试</span>
                                    </div>
                                </a>
                            </li>
                            <li><a class="video_wrap" href="#">
                                <img src="<%=path%>/images/sitem01_01.jpg">
                            </a>
                                <div class="zxks survey-item-txtbox">
                                    <h2 class="survey-item-txtname">高空作业视频</h2>
                                    <div class="clearfix"></div>
                                    <span class="txtboxspanL">45 道题</span> <span class="txtboxspanR">在线考试</span>
                                </div>
                            </li>
                            <li><a class="video_wrap" href="#">
                                <img src="<%=path%>/images/sitem01_01.jpg">
                            </a>
                                <div class="zxks survey-item-txtbox">
                                    <h2 class="survey-item-txtname">高空作业视频</h2>
                                    <div class="clearfix"></div>
                                    <span class="txtboxspanL">45 道题</span> <span class="txtboxspanR">在线考试</span>
                                </div>
                            </li>
                            <li><a class="video_wrap" href="#">
                                <img src="<%=path%>/images/sitem01_01.jpg">
                            </a>
                                <div class="zxks survey-item-txtbox">
                                    <h2 class="survey-item-txtname">高空作业视频</h2>
                                    <div class="clearfix"></div>
                                    <span class="txtboxspanL">45 道题</span> <span class="txtboxspanR">在线考试</span>
                                </div>
                            </li>
                            <li><a class="video_wrap" href="#">
                                <img src="<%=path%>/images/sitem01_01.jpg">
                            </a>
                                <div class="zxks survey-item-txtbox">
                                    <h2 class="survey-item-txtname">高空作业视频</h2>
                                    <div class="clearfix"></div>
                                    <span class="txtboxspanL">45 道题</span> <span class="txtboxspanR">在线考试</span>
                                </div>
                            </li>
                            <li><a class="video_wrap" href="#">
                                <img src="<%=path%>/images/sitem01_01.jpg">
                            </a>
                                <div class="zxks survey-item-txtbox">
                                    <h2 class="survey-item-txtname">高空作业视频</h2>
                                    <div class="clearfix"></div>
                                    <span class="txtboxspanL">45 道题</span> <span class="txtboxspanR">在线考试</span>
                                </div>
                            </li>
                            <li><a class="video_wrap" href="#">
                                <img src="<%=path%>/images/sitem01_01.jpg">
                            </a>
                                <div class="zxks survey-item-txtbox">
                                    <h2 class="survey-item-txtname">高空作业视频</h2>
                                    <div class="clearfix"></div>
                                    <span class="txtboxspanL">45 道题</span> <span class="txtboxspanR">在线考试</span>
                                </div>
                            </li>
                            <li><a class="video_wrap" href="#">
                                <img src="<%=path%>/images/sitem01_01.jpg">
                            </a>
                                <div class="zxks survey-item-txtbox">
                                    <h2 class="survey-item-txtname">高空作业视频</h2>
                                    <div class="clearfix"></div>
                                    <span class="txtboxspanL">45 道题</span> <span class="txtboxspanR">在线考试</span>
                                </div>
                            </li>
                            <div class="clearfix"></div>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div style="height:280px;"></div>
</div>
<div id="Wfooter" style="height:320px;">
    <div id="Wcontact">
        <ul class="W-ulCons" style="border:0; padding:0; width:206px">
            <img src="<%=path%>/images/sojumpwjxlogonew.png" width="121" style="display:block;"/>
        </ul>
        <ul class="W-ulCons">
            <li class="i-Common WulCons-i1">
                <a href="index.do">首页</a></li>

        </ul>
        <ul class="W-ulCons">
            <li class="i-Common WulCons-i2">
                <a href="toCourseware.do">课件中心</a></li>


        </ul>
        <ul class="W-ulCons">
            <li class="i-Common WulCons-i3">
                <a href="toTiku.do">在线考试</a></li>

        </ul>
        <ul class="W-ulCons" style="width:165px; float:right;">
            <li class="i-Common WulCons-i4"><a href="toUser.do">个人中心</a></li>
        </ul>
    </div>
    <div class="W-coypright">
        <ul class="WalCons W-fttx">
            <li><span class="W-Rttx">Contact US</span><span class="W-Rttx">Tel：400-839-8080</span><span class="W-Rttx">E-mail：cs@sojump.com</span>
            </li>
        </ul>

        <ul class="WalCons Walcons_txt">
            <li class="W-mark">
                <img width="210" height="25" alt="" src="//image.sojump.com/images/MasterNew/bottom_right.gif"><br>
                监理工程师在线课程
            </li>
        </ul>
    </div>

</div>
</body>
</html>
