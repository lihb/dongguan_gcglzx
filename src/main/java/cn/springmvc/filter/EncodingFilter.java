package cn.springmvc.filter;

import javax.servlet.*;
import java.io.IOException;


public class EncodingFilter implements Filter {
	
	private FilterConfig config;
	private String encoding="UTF-8";
	
	public void init(FilterConfig config)throws ServletException {
		this.config=config;
		String s=config.getInitParameter("encoding");
		if(s!=null){
			this.encoding=s;
		}
	}

	
	public void doFilter(ServletRequest request, ServletResponse response,
						 FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);
		chain.doFilter(request, response);
	}


	public void destroy() {
		// TODO Auto-generated method stub
		config=null;
	}

}
