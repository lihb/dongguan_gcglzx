package cn.springmvc.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogFilter implements Filter {

	private static final Logger logger = Logger.getLogger("LogFilter");
	private String filterName;

	public void init(FilterConfig config) throws ServletException {

		// 获取 Filter 的 name，配置在 web.xml 中
		filterName = config.getFilterName();

		logger.info("启动 Filter:" + filterName);

	}

	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		long startTime = System.currentTimeMillis();
		String requestURI = request.getRequestURI();

		requestURI = request.getQueryString() == null ? requestURI
				: (requestURI + "?" + request.getQueryString());

		chain.doFilter(request, response);

		long endTime = System.currentTimeMillis();

		logger.info(request.getRemoteAddr() + " 访问了 " + requestURI + ", 总用时 "
				+ (endTime - startTime) + " 毫秒。");

	}

	public void destroy() {
		logger.info("关闭 Filter: " + filterName);
	}

}
