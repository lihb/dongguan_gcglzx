package cn.springmvc.filter;

import cn.springmvc.common.ApiCommon;
import cn.springmvc.entity.User;
import cn.springmvc.service.VerifyService;
import cn.springmvc.util.JedisClient;
import cn.springmvc.util.ResultHelper;
import com.alibaba.druid.util.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;
import redis.clients.jedis.Jedis;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class SessionFilter extends OncePerRequestFilter {

    private static final Logger logger = Logger.getLogger("SessionFilter");

    //	@Autowired
    private VerifyService verifyService;
    //	@Autowired
    private ApiCommon apiCommon;

    public VerifyService getVerifyService() {
        return verifyService;
    }

    public void setVerifyService(VerifyService verifyService) {
        this.verifyService = verifyService;
    }

    public ApiCommon getApiCommon() {
        return apiCommon;
    }

    public void setApiCommon(ApiCommon apiCommon) {
        this.apiCommon = apiCommon;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        //不拦截的请求
        String[] notFilter = new String[]{"user/index", "user/toLogin", "user/login", "user/toRegister", "wap/show", "wap/thumb", "down/apk", "wx/store"};
        boolean doFilter = true;
        //请求URL
        String uri = request.getRequestURI();
        for (String s : notFilter) {
            //返回匹配字符第一个字母下标
            if (uri.indexOf(s) != -1) {
                doFilter = false;
            }
        }

        if (doFilter) {
            logger.info(Thread.currentThread().getName() + "----验证用户信息开始----");
            int resultCode = 0;
            // 获取header值和requestURI，用来session会话校验
            User user = (User) request.getSession().getAttribute("user");
            String sessionKey = (String) request.getSession().getAttribute("sessionKey");
            String sessionSecretClient = (String) request.getSession().getAttribute("sessionSecret");
            logger.info(Thread.currentThread().getName() + "--------------获取session值开始----------");
            logger.info(Thread.currentThread().getName() + "传入的参数为sessionKey： " + sessionKey);
            logger.info(Thread.currentThread().getName() + "传入的参数为sessionSecretClient： " + sessionSecretClient);
            logger.info(Thread.currentThread().getName() + "---------获取session值完毕------------");

            // 验证用户信息
            Jedis jedis = JedisClient.getJedis();
            int userId = user.getUid();
            String sessionSecret = jedis.hget(String.valueOf(userId), "sessionSecret");
            logger.info(Thread.currentThread().getName() + " userId-->" + userId + " 从redis中获取得到的sessionSecret---" + sessionSecret);
            if (!jedis.exists(String.valueOf(userId))|| !StringUtils.equals(sessionSecretClient, sessionSecret)) {
                resultCode = 105;
                ResultHelper.returnJsonResult(response, resultCode);
                logger.info(Thread.currentThread().getName() + " sessionkey 已经失效！！");
                return;
            }
            logger.info(Thread.currentThread().getName() + " 验证用户信息完毕");
            //刷新session的有效时间
//                jedis.expire(String.valueOf(userId), apiCommon.expireTime);


        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }
}
