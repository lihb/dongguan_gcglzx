package cn.springmvc.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class JedisClient {

	public static Jedis getJedis() {

		JedisPool pool;
		Jedis jedis = null;
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(100000);
		config.setMaxIdle(20);
		config.setMaxWaitMillis(100000);
		config.setTimeBetweenEvictionRunsMillis(200); // 设置自动回收作废连接的时间

//		pool = new JedisPool(config, "42.123.76.129", 6379, 10000); // 加大过期时间，确保redis能正确写入、读取数据
		pool = new JedisPool(config, "127.0.0.1", 6379, 100000); // 加大过期时间，确保redis能正确写入、读取数据

		boolean borrowOrOprSuccess = true;
		try {
			jedis = pool.getResource();
			// do redis opt by instance
		} catch (JedisConnectionException e) {
			borrowOrOprSuccess = false;
			if (jedis != null){
				pool.returnBrokenResource(jedis);
			}

		} finally {
			if (borrowOrOprSuccess){
				pool.returnResource(jedis);
			}
		}
		// jedis = pool.getResource();

		return jedis;
	}

	public static void main(String[] args) {

		for (int i = 0; i < 2000; i++) { // 测试5000个并发，去调用redis
			final int num = i;
			new Thread(new Runnable() {
				public void run() {

					Jedis jedis = JedisClient.getJedis();
					jedis.set(Thread.currentThread().getName() + "records",
							"aaa" + num);
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(jedis.get(Thread.currentThread()
							.getName() + "records"));
					jedis.del(Thread.currentThread().getName() + "records");
				}
			}).start();

		}
	}
}
