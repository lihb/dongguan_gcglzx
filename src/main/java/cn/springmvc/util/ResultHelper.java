package cn.springmvc.util;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


/**
 * @author lhb
 *  封装响应参数
 */
public class ResultHelper {
	
	/**Json响应参数包含object
	 * @param response
	 * @param result
	 * @param object
	 */
	public static void returnJsonResult(HttpServletResponse response,int result,Object object){
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("status", getMsg(result));
		map.put("result", result);
	    map.put("list",object);
		AjaxResponseUtil.returnJsonData(response, map);
	}
	
	/**Json响应参数包含object 与  Object计数统计
	 * @param response
	 * @param result
	 * @param object
	 * @param total  
	 */
	public static void returnJsonResultTotal(HttpServletResponse response,int result,Object object,int total){
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("status", getMsg(result));
		map.put("result", result);
	    map.put("list",object);
	    map.put("total",total);
		AjaxResponseUtil.returnJsonData(response, map);
	}
	
	
	/**Json响应参数只含结果码
	 * @param response
	 * @param result
	 */
	public static void returnJsonResult(HttpServletResponse response,int result){
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("status", getMsg(result));
		map.put("result", result);
		AjaxResponseUtil.returnJsonData(response, map);
	}
	
	/**Text响应参数只含结果码
	 * @param response
	 * @param result
	 */
	public static void returnTextResult(HttpServletResponse response,int result){
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("status", getMsg(result));
		map.put("result", result);
		AjaxResponseUtil.returnTextData(response, map);
	}
	
	
	/**
	 * @param result
	 * @return 	
	 * 0  操作成功
	 * 1 Session未保存用户ID,需要重定向至login.html
	 * 2 未获取请求参数
	 * -1 传入时间格式异常
	 */
	public static String getMsg(int result){
		if(result == 0){
			return "Successed";
		}
		if(result == 100){
		    return "Error";
	    }
	    if(result == 101){
	    	return "InvalidAccount";
	    }
	    if(result == 102){
	    	return "InvalidPassword";	    			
	    }
	    if(result == 103){
	    	return "InvalidToken";	    			
	    }
	    if(result == 104){
	    	return "NoPermission";	    			
	    }
	    if(result == 105){
		    return "ExpireSessionkey";
	    }
	    if(result == 106){
		    return "MismatchSignature";
	    }
	    if(result == 107){
		    return "NetworkError";
	    }
	    if (result == 200) {
			return "AlbumExisted";
		}
	    if (result == 201) {
			return "AlbumNotExisted";
		}
	    if (result == 202) {
			return "RepeatRename";
		}
	    if (result == 203) {
			return "NoPhotoInAlbum";
		}
	    if (result == 300) {
			return "PhotoExisted";
		}
	    if (result == 301) {
			return "PhotoNotExist";
		}
	    if (result == 400) {
			return "Exist";
		}
	    if (result == 401) {
			return "No";
		}
	    if (result == 500) {
			return "InvalidPathUrl";
		}
	    if(result < 0){
		    return "InvalidPassword";
	    }
	    return "Error!";
	}
		

	
	//视图请求
//	
//	if(!StringUtil.isEmpty(retStr)&&StringUtil.isJsonObject(retStr)){
//		JSONObject object = JSONObject.fromObject(retStr);
//		if(object.has("result")&&0==object.getInt("result")){
//			model.addAttribute("coin", object.getString("coin"));
//			model.addAttribute("cardLackFlow", object.getString("cardLackFlow"));
//			model.addAttribute("cardFlow", object.getString("cardFlow"));
//			model.addAttribute("monthExchange", object.getString("monthExchange"));
//			model.addAttribute("monthDemand", object.getString("monthDemand"));
//			model.addAttribute("monthGrant", object.getString("monthGrant"));
//			model.addAttribute("monthEarned", object.getString("monthEarned"));
//			String imgUrl = "/source/img/"+object.getString("cardFlow")+".png";
//			model.addAttribute("imgUrl", imgUrl);
//		}
//	}

}
