package cn.springmvc.util;

import java.util.Random;

public class MD5ToDir {
	
	/*得到md5值*/
	public static String getMD5(byte[] source) {  
        String s = null;  
        char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };  
        try {  
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");  
            md.update(source);  
            byte tmp[] = md.digest();   
            char str[] = new char[16 * 2];   
            int k = 0;   
            for (int i = 0; i < 16; i++) {   
                byte byte0 = tmp[i];   
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];   
                str[k++] = hexDigits[byte0 & 0xf];   
            }  
            s = new String(str);   
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return s;  
    }  
	
	/** 产生一个随机的字符串*/  
	public static String randomString(int length) {  
	    String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
	    Random random = new Random();  
	    StringBuffer buf = new StringBuffer();  
	    for (int i = 0; i < length; i++) {  
	        int num = random.nextInt(62);  
	        buf.append(str.charAt(num));  
	    }  
	    return buf.toString();  
	}  
	
	/* 创建目录路径*/
	public static String mkMultiDir(){
		StringBuilder sb = new StringBuilder();
		String md5str = randomString(8);
		md5str = getMD5(md5str.getBytes());
		System.out.println(md5str);
		sb.append("/").append(md5str.substring(0,2)).append("/")
		.append(md5str.substring(2,4)).append("/").append(md5str.substring(4,6));
		System.out.println(sb.toString());
		//File dirStr = new File(sb.toString());
		//dirStr.mkdirs();
		return sb.toString();
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(getMD5("abcdei".getBytes()).toUpperCase());
//		String str = getMD5("abcdefgh".getBytes());
	//	System.out.println("---start---");
		//for (int i = 0; i < 10000; i++) {
//			String str = RandomString(8);
//			str = getMD5(str.getBytes());
//			System.out.println(str);
			mkMultiDir();
		//}
	//	System.out.println("-----end----");
	//	
		

	}

}
