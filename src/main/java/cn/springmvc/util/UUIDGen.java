package cn.springmvc.util;

import java.util.UUID;

public class UUIDGen {

	public static String getUUID() {

		return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
	}
	
	public static void main(String[] args) {
//		System.out.println(getUUID());
		System.out.print(testReturn(5, 2));

	}

	private static int testReturn(int a, int b) {
		int result = 0;
		try {
			result = a / b;
			return result;
		} catch (Exception e) {
			System.out.println("e = " + e);
			return -100;
		} finally {
			System.out.println("finally block");
			return 100;
		}
	}

}
