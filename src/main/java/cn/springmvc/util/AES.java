package cn.springmvc.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by lhb on 14-11-22.
 */
public class AES {

    public static final String VIPARA = "0102030405060708";
    public static final String BM = "UTF-8";

    public static String encrypt(String dataPassword, String cleartext)
            throws Exception {
        IvParameterSpec zeroIv = new IvParameterSpec(VIPARA.getBytes());
        SecretKeySpec key = new SecretKeySpec(dataPassword.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
        byte[] encryptedData = cipher.doFinal(cleartext.getBytes(BM));

        return Base64.encode(encryptedData);
    }

    public static String decrypt(String dataPassword, String encrypted)
            throws Exception {
        byte[] byteMi = Base64.decode(encrypted);
        IvParameterSpec zeroIv = new IvParameterSpec(VIPARA.getBytes());
        SecretKeySpec key = new SecretKeySpec(dataPassword.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);
        byte[] decryptedData = cipher.doFinal(byteMi);

        return new String(decryptedData,BM);
    }

    public static void main(String[] args) {
        String content = "18122031501";
        String key = "EF8BA7A2AB5E4F08";
        System.out.println("key的长度 = " + key.length());

        System.out.println("加密前数据content = " + content);

        // 加密
        String data = "";
        try {
            data = AES.encrypt(key, content);
            System.out.println("加密后数据data = " + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 解密
        try {
            String res = AES.decrypt(key, data);
            System.out.println("解密后数据res = " + res);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
