package cn.springmvc.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @author EternalSpring
 *  将封装好的返回结构JSON字符化，并传送至前端
 */
public class AjaxResponseUtil {
	
	
	public static void returnJsonData(HttpServletResponse response , Map<String,Object> responseData){
		
		String resultString=createJson(responseData);
		try{
			//跨域报头
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setContentType("application/json; charset=UTF-8");
			response.getWriter().write(resultString);
			response.getWriter().flush();
			response.getWriter().close();
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	
	public static void returnTextData(HttpServletResponse response,Map<String,Object>responseData){
		String resultString=createJson(responseData);
		try{
			//跨域报头
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setContentType("text/html; charset=UTF-8");
			response.getWriter().write(resultString);
			response.getWriter().flush();
			response.getWriter().close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static String createJson(Map<String,Object> responseData){
		return JSON.toJSONString(responseData, SerializerFeature.PrettyFormat);
//		return JSON.toJSONString(responseData);

	}
	
}
