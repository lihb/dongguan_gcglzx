package cn.springmvc.controller;

import cn.springmvc.entity.User;
import cn.springmvc.service.UserService;
import cn.springmvc.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
@RequestMapping("/")
public class MainController {

	@Autowired
	private UserService userService;

	@RequestMapping("test.do")
	public String index(){
		System.out.println("in test.do");
//		User user = new User();
//		user.setNickname("小3");
//		user.setAccount("xxxx");
//		user.setAvatar("http://xx.yy.xom");
//		user.setCreateTime(new Date(2017-1900,02,25));
//		user.setSelfIntro("自我介紹");
//		user.setPassword("password");
//		System.out.println(userService.insertUser(user));
		return "test";
	}
	
}
