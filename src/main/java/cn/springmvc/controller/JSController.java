package cn.springmvc.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/js")
public class JSController {
	private static final Logger logger = Logger.getLogger("JSController");

	@RequestMapping("/demo.do")
	public String index(){
		return "demo";
	}


}
