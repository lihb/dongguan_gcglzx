package cn.springmvc.controller;

import cn.springmvc.entity.Question;
import cn.springmvc.entity.User;
import cn.springmvc.service.QuestionService;
import cn.springmvc.util.ResultHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2017/5/30.
 */

@Controller
@RequestMapping(value = "/question")
public class QuestionController {
    private static final Logger logger = Logger.getLogger("QuestionController");

    @Autowired
    private QuestionService questionService;

    @RequestMapping("/getDataByType.do")
    public void getDataByType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int resultCode = 0;
//        int type = Integer.parseInt(request.getParameter("type"));
        List<Question> questionList = questionService.selectData(1);
        if (null == questionList) {
            resultCode = 203;
            ResultHelper.returnJsonResult(response, resultCode);
            return;
        }
        ResultHelper.returnJsonResult(response, resultCode, questionList);
    }

    @RequestMapping(value = "/computerScore.do", method = RequestMethod.POST)
    public void computerScore(@RequestParam(value = "choices[]")  String[]  choices, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int resultCode = 0;
        int score = 0;
        List<Question> questionList = questionService.selectData(1);
        if (null == questionList) {
            resultCode = 203;
            ResultHelper.returnJsonResult(response, resultCode);
            return;
        }
        for (int i = 0; i < questionList.size(); i++) {
            Question question = questionList.get(i);
            if (question.getAnswer().equals(choices[i])) {
                score += question.getScore();
            }
        }
        User user = (User) request.getSession().getAttribute("user");
        if (null != user) {

        }
        ResultHelper.returnJsonResult(response, resultCode, score);
    }
}
