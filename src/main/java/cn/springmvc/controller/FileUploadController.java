package cn.springmvc.controller;

import cn.springmvc.service.QuestionService;
import cn.springmvc.util.ResultHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * Created by lhb on 2017/4/14.
 */
@Controller
@RequestMapping("/file")
public class FileUploadController {
    private Logger logger = Logger.getLogger("FileUploadController");


    @Autowired
    private QuestionService questionService;

    @RequestMapping("upload.do")
    public String index(){
        return "fileupload";
    }

    @RequestMapping("/processupload.do")
    public void upload(@RequestParam("file") MultipartFile file,
                       HttpServletRequest request, HttpServletResponse response) throws Exception{

        int resultCode = 0;

        if ((null == file) || file.isEmpty()) {
            resultCode = 100;
            ResultHelper.returnJsonResult(response, resultCode);
            return ;
        }
        String originalFileName = null;
        // 上传文件到服务器
        try {
            originalFileName = file.getOriginalFilename();

            // 插入试题到试题库
            questionService.insertData(file.getInputStream());
            String fileExt = originalFileName.substring(
                    originalFileName.lastIndexOf(".") + 1).toLowerCase();
            logger.info("thread---"+Thread.currentThread()+"---文件名==" + originalFileName);
            String dirStr = request.getSession().getServletContext().getRealPath("/") + "upload/";
            File dir = new File(dirStr);
            dir.mkdirs();
            String newName = dirStr + originalFileName.substring(0,originalFileName.lastIndexOf(".")) + System.currentTimeMillis() + "." + fileExt;
            File f = new File(newName);
            file.transferTo(f);
        }catch (Exception e){
            e.printStackTrace();
        }
        finally{

        }


    }
}
