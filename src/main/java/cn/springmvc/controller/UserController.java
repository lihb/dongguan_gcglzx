package cn.springmvc.controller;

import cn.springmvc.common.ApiCommon;
import cn.springmvc.entity.Question;
import cn.springmvc.entity.User;
import cn.springmvc.service.QuestionService;
import cn.springmvc.service.UserService;
import cn.springmvc.service.VerifyService;
import cn.springmvc.util.JedisClient;
import cn.springmvc.util.ResultHelper;
import cn.springmvc.util.UUIDGen;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	private static final Logger logger = Logger.getLogger("UserController");

	@Autowired
	private ApiCommon apiCommon;

	@Autowired
	private User userInfo;

	@Autowired
	private UserService userService;
	
	@Autowired
	private VerifyService verifyService;

	@Autowired
	private QuestionService questionService;

	@RequestMapping("/index.do")
	public String toIndexJsp(){
		return "index";
	}

	@RequestMapping("/toLogin.do")
	public String toLoginJsp(){
		return "login";
	}

	@RequestMapping("/toRegister.do")
	public String toRegisterJsp(){
		return "register";
	}

	@RequestMapping("/toUser.do")
	public String toUserJsp(){
		return "user";
	}

	@RequestMapping("/toTiku.do")
	public String toTikuJsp(){
		return "tiku";
	}

	@RequestMapping("/toCourseware.do")
	public String toCoursewareJsp(){
		return "courseware";
	}

	@RequestMapping("/toExamination.do")
	public String toExaminationJsp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Question> questionList = questionService.selectData(1);
		request.setAttribute("questionList", questionList);
		return "examination";
	}

	@RequestMapping("/toVideo.do")
	public String toVideoJsp(){
		return "video";
	}

	@RequestMapping(value = "/login.do")
	public String login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int resultCode = 0; // 状态码
		
		logger.info(Thread.currentThread().getName() + "-----------------用户登录操作开始------------");

		// 获取客户端参数
		logger.info(Thread.currentThread().getName() + "-----------------获取参数------------");
		String account = request.getParameter("username");
		String password = request.getParameter("password");
		logger.info(Thread.currentThread().getName() + " 传入的参数为account： " + account);

		logger.info(Thread.currentThread().getName() + "----------------获取参数完毕----------");

		// 手机号检测
		if(!verifyService.isPhoneNum(account)){
			resultCode = 101; // 不是手机号码
			ResultHelper.returnJsonResult(response, resultCode);
			return "error";
		}


		// 利用UUID生成sessionKey和sessionSecret
		String sessionKey = UUIDGen.getUUID();
		String sessionSecret = UUIDGen.getUUID();

		// userInfo.setSessionKey(sessionKey);
		// userInfo.setSessionSecret(sessionSecret);
		logger.info(Thread.currentThread().getName() + "---->sessionKey and sessionSecret" + sessionKey
				+ "<------");

		/*
		 * 查询数据库，该用户是否是第一次登录，如果是第一次登录，则创建新用户
		 */
		Object result = userService.queryUserByAccount(account);
		if (result == null || !((User) result).getPassword().equals(password)) {
			logger.info("登录失败");
			String message = String.format(
			"对不起，用户名或密码有误！！请重新登录！2秒后为您自动跳到登录页面！！<meta http-equiv='refresh' content='2;url=%s'", request.getContextPath()+"/user/index.do");
			request.setAttribute("message",message);
//			request.getRequestDispatcher("message").forward(request, response);
			return "message";

		}
		userInfo = (User) result;
		if (userInfo.getPassword().equals(password)) {
			logger.info("登录成功");
			request.getSession().setAttribute("user", userInfo);
			request.getSession().setAttribute("sessionKey", sessionKey);
			request.getSession().setAttribute("sessionSecret", sessionSecret);
			request.getSession().setAttribute("expirationTime", apiCommon.expireTime);
			//设置cookie
			Cookie cookie_userId = new Cookie("nickname",
					String.valueOf(userInfo.getNickname()));
			cookie_userId.setMaxAge(3600 * 24 * 30);
			cookie_userId.setPath("/");
			response.addCookie(cookie_userId);

		}
		// 写入到redis中
		Jedis jedis = JedisClient.getJedis();
		jedis.hset(String.valueOf(userInfo.getUid()), "sessionKey", sessionKey);
		jedis.hset(String.valueOf(userInfo.getUid()), "sessionSecret", sessionSecret);
		jedis.expire(String.valueOf(userInfo.getUid()), apiCommon.expireTime); // 设置session的过期时间为24小时

		// 机型、系统等信息写入日志文件
		Map<String, Object> logmap = new HashMap<String, Object>();
//		logmap.put("clientType", clientType);
//		logmap.put("clientVersion", clientVersion);
//		logmap.put("osFamily", osFamily);
//		logmap.put("osVersion", osVersion);
		logmap.put("sessionKey", sessionKey);
		logmap.put("sessionSecret", sessionSecret);
		logger.info(Thread.currentThread().getName() + JSONObject.toJSON(logmap));

//		String message = String.format(
//				"恭喜：%s,登陆成功！本页将在1秒后跳到首页！！<meta http-equiv='refresh' content='1;url=%s'",
//				userInfo.getNickname(),
//				request.getContextPath()+"/user/toUser.do");
//		request.setAttribute("message",message);
		logger.info(Thread.currentThread().getName() + "-----------------用户登录操作结束------------");
		return "user";

//		// 构造返回map
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("account", account);
//		map.put("sessionKey", sessionKey);
//		map.put("sessionSecret", sessionSecret);
//		ResultHelper.returnJsonResult(response, resultCode, map);

	}

	@RequestMapping(value = "/register.do")
	public String register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int resultCode = 0; // 状态码

		logger.info(Thread.currentThread().getName() + "-----------------用户注册操作开始------------");

		// 获取客户端参数
		logger.info(Thread.currentThread().getName() + "-----------------获取参数------------");
		String nickName = request.getParameter("register_username");
		String password = request.getParameter("register_password");
		String username = request.getParameter("register_phone_number");
		logger.info(Thread.currentThread().getName() + " 传入的参数为nickName： " + nickName);

		logger.info(Thread.currentThread().getName() + "----------------获取参数完毕----------");
		if (StringUtils.isEmpty(username) || StringUtils.isEmpty(nickName)  || StringUtils.isEmpty(password)) {
			logger.info(Thread.currentThread().getName() + "----------------error to register.jsp----------");
			return "register";
		}
		Object result = userService.queryUserByAccount(username);
		if (null != result) {
			logger.info(Thread.currentThread().getName() + "----------------该手机号已经注册----------");
		}
		User user = new User();
		user.setPassword(password);
		user.setCreateTime(new Date());
		user.setNickname(nickName);
		user.setUsername(username);
		int count = userService.insertUser(user);
		if (count == 1) {
			logger.info(Thread.currentThread().getName() + "----------------用户注册成功---------");
		}
		return "login";
	}
}
