package cn.springmvc.dao;

import cn.springmvc.entity.PaperQuestion;

public interface PaperQuestionMapper {
    int deleteByPrimaryKey(Integer pqid);

    int insert(PaperQuestion record);

    int insertSelective(PaperQuestion record);

    PaperQuestion selectByPrimaryKey(Integer pqid);

    int updateByPrimaryKeySelective(PaperQuestion record);

    int updateByPrimaryKey(PaperQuestion record);
}