package cn.springmvc.dao;

import cn.springmvc.entity.UserCourse;

import java.util.List;

public interface UserCourseMapper {
    int deleteByPrimaryKey(Integer ucid);

    int insert(UserCourse record);

    int insertSelective(UserCourse record);

    UserCourse selectByPrimaryKey(Integer ucid);

    int updateByPrimaryKeySelective(UserCourse record);

    int updateByPrimaryKey(UserCourse record);


    List<UserCourse> getUserCourseByUserId(int userId);

    List<UserCourse> getUserCourseByCourseId(int courseId);
}