package cn.springmvc.dao;

import cn.springmvc.entity.CourseWare;

public interface CourseWareMapper {
    int deleteByPrimaryKey(Integer cwid);

    int insert(CourseWare record);

    int insertSelective(CourseWare record);

    CourseWare selectByPrimaryKey(Integer cwid);

    int updateByPrimaryKeySelective(CourseWare record);

    int updateByPrimaryKey(CourseWare record);
}