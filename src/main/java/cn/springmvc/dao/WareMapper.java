package cn.springmvc.dao;

import cn.springmvc.entity.Ware;

public interface WareMapper {
    int deleteByPrimaryKey(Integer wid);

    int insert(Ware record);

    int insertSelective(Ware record);

    Ware selectByPrimaryKey(Integer wid);

    int updateByPrimaryKeySelective(Ware record);

    int updateByPrimaryKey(Ware record);
}