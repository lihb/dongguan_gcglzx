package cn.springmvc.dao;

import cn.springmvc.entity.UserPaper;

public interface UserPaperMapper {
    int deleteByPrimaryKey(Integer upid);

    int insert(UserPaper record);

    int insertSelective(UserPaper record);

    UserPaper selectByPrimaryKey(Integer upid);

    int updateByPrimaryKeySelective(UserPaper record);

    int updateByPrimaryKey(UserPaper record);
}