package cn.springmvc.dao;

import cn.springmvc.entity.CoursePaper;

public interface CoursePaperMapper {
    int deleteByPrimaryKey(Integer cpid);

    int insert(CoursePaper record);

    int insertSelective(CoursePaper record);

    CoursePaper selectByPrimaryKey(Integer cpid);

    int updateByPrimaryKeySelective(CoursePaper record);

    int updateByPrimaryKey(CoursePaper record);
}