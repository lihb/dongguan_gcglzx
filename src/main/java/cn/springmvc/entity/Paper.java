package cn.springmvc.entity;

public class Paper {
    private Integer pid;

    private Integer pname;

    private Integer passscore;

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getPname() {
        return pname;
    }

    public void setPname(Integer pname) {
        this.pname = pname;
    }

    public Integer getPassscore() {
        return passscore;
    }

    public void setPassscore(Integer passscore) {
        this.passscore = passscore;
    }
}