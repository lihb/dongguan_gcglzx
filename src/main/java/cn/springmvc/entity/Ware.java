package cn.springmvc.entity;

public class Ware {
    private Integer wid;

    private String wname;

    private String wstatus;

    private String worigin;

    private Integer wtime;

    private Integer wcredit;

    private String whandout;

    private Integer wcategory;

    public Integer getWid() {
        return wid;
    }

    public void setWid(Integer wid) {
        this.wid = wid;
    }

    public String getWname() {
        return wname;
    }

    public void setWname(String wname) {
        this.wname = wname == null ? null : wname.trim();
    }

    public String getWstatus() {
        return wstatus;
    }

    public void setWstatus(String wstatus) {
        this.wstatus = wstatus == null ? null : wstatus.trim();
    }

    public String getWorigin() {
        return worigin;
    }

    public void setWorigin(String worigin) {
        this.worigin = worigin == null ? null : worigin.trim();
    }

    public Integer getWtime() {
        return wtime;
    }

    public void setWtime(Integer wtime) {
        this.wtime = wtime;
    }

    public Integer getWcredit() {
        return wcredit;
    }

    public void setWcredit(Integer wcredit) {
        this.wcredit = wcredit;
    }

    public String getWhandout() {
        return whandout;
    }

    public void setWhandout(String whandout) {
        this.whandout = whandout == null ? null : whandout.trim();
    }

    public Integer getWcategory() {
        return wcategory;
    }

    public void setWcategory(Integer wcategory) {
        this.wcategory = wcategory;
    }
}