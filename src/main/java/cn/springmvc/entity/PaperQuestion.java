package cn.springmvc.entity;

public class PaperQuestion {
    private Integer pqid;

    private Integer pid;

    private Integer qid;

    private Paper paper;

    private Question question;

    public Integer getPqid() {
        return pqid;
    }

    public void setPqid(Integer pqid) {
        this.pqid = pqid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getQid() {
        return qid;
    }

    public void setQid(Integer qid) {
        this.qid = qid;
    }

    public Paper getPaper() {
        return paper;
    }

    public void setPaper(Paper paper) {
        this.paper = paper;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}