package cn.springmvc.entity;

public class Course {
    private Integer cid;

    private String cname;

    private String ctype;

    private String cattach;

    private Integer cpasscore;

    private Boolean celective;

    private Integer cpermission;

    private Integer ctag;

    private Integer ctime;

    private String ccredit;

    private String ccover;

    private String cdesc;

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype == null ? null : ctype.trim();
    }

    public String getCattach() {
        return cattach;
    }

    public void setCattach(String cattach) {
        this.cattach = cattach == null ? null : cattach.trim();
    }

    public Integer getCpasscore() {
        return cpasscore;
    }

    public void setCpasscore(Integer cpasscore) {
        this.cpasscore = cpasscore;
    }

    public Boolean getCelective() {
        return celective;
    }

    public void setCelective(Boolean celective) {
        this.celective = celective;
    }

    public Integer getCpermission() {
        return cpermission;
    }

    public void setCpermission(Integer cpermission) {
        this.cpermission = cpermission;
    }

    public Integer getCtag() {
        return ctag;
    }

    public void setCtag(Integer ctag) {
        this.ctag = ctag;
    }

    public Integer getCtime() {
        return ctime;
    }

    public void setCtime(Integer ctime) {
        this.ctime = ctime;
    }

    public String getCcredit() {
        return ccredit;
    }

    public void setCcredit(String ccredit) {
        this.ccredit = ccredit == null ? null : ccredit.trim();
    }

    public String getCcover() {
        return ccover;
    }

    public void setCcover(String ccover) {
        this.ccover = ccover == null ? null : ccover.trim();
    }

    public String getCdesc() {
        return cdesc;
    }

    public void setCdesc(String cdesc) {
        this.cdesc = cdesc == null ? null : cdesc.trim();
    }

    @Override
    public String toString() {
        return "Course{" +
                "cid=" + cid +
                ", cname='" + cname + '\'' +
                ", ctype='" + ctype + '\'' +
                ", cattach='" + cattach + '\'' +
                ", cpasscore=" + cpasscore +
                ", celective=" + celective +
                ", cpermission=" + cpermission +
                ", ctag=" + ctag +
                ", ctime=" + ctime +
                ", ccredit='" + ccredit + '\'' +
                ", ccover='" + ccover + '\'' +
                ", cdesc='" + cdesc + '\'' +
                '}';
    }
}