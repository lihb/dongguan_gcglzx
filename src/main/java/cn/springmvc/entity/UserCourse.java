package cn.springmvc.entity;

import java.util.List;

public class UserCourse {
    private Integer ucid;

    private Integer uid;

    private Integer cid;

    private User user;

    private Course course;

    private Integer status;

    private List<User> userList;

    private List<Course> courseList;

    public Integer getUcid() {
        return ucid;
    }

    public void setUcid(Integer ucid) {
        this.ucid = ucid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    @Override
    public String toString() {
        return "UserCourse{" +
                "ucid=" + ucid +
                ", uid=" + uid +
                ", cid=" + cid +
                ", user=" + user +
                ", course=" + course +
                ", status=" + status +
                ", userList=" + userList +
                ", courseList=" + courseList +
                '}';
    }
}