package cn.springmvc.service;

public interface VerifyService {

	// 验证userId和accessToken
	public boolean verifyAccount(String sessionKey, String operate,
                                 String requestURI, String date, int userId, String signature);

	// // 验证ownerId和AlbumName
	// public boolean verifyAlbum(int ownerId, long floderId);

	// 判断用户userId是否为folderId的拥有人
//	public boolean isOwner(Integer userId, long folderId);

	// 判断用户userId是否为folderId的分享人
//	public boolean isSharer(Integer userId, long folderId);

	// 验证传入参数是否是正确的手机号码
	public boolean isPhoneNum(String account);

}
