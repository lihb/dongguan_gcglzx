package cn.springmvc.service;

import cn.springmvc.entity.UserCourse;

import java.util.List;

/**
 * Created by Administrator on 2017/4/13.
 */
public interface UserCourseService {

    /**
     * 查看某个用户所修的全部课程
     * @param userId 用户id
     * @return
     */
    List<UserCourse> getUserCourseByUserId(int userId);

    /**
     * 查看上了某个课程的全部人员
     * @param courseId 课程id
     * @return
     */
    List<UserCourse> getUserCourseByCourseId(int courseId);


    int insert(UserCourse record);
}
