package cn.springmvc.service.impl;

import cn.springmvc.dao.QuestionMapper;
import cn.springmvc.entity.Question;
import cn.springmvc.service.QuestionService;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lhb on 2017/4/14.
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    private static final Logger logger = Logger.getLogger("QuestionServiceImpl");

    @Autowired
    private QuestionMapper questionMapper;

    public void insertData(InputStream is) {
        List<Question> questionList = new ArrayList<Question>();
        try {
           questionList =  readBrandPeriodSorXls(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!questionList.isEmpty()) {
            int res = -1;
            res = questionMapper.insertBatch(questionList);
            if (-1 == res) {
                logger.info("-------批量导入试题失败------");
            }
            logger.info(Thread.currentThread().getName() + "------批量导入试题成功----结束-----");

        }else {
            System.out.println("questionList is empty. ");
        }
    }

    public List<Question> selectData(int type) {
        List<Question> questionList = new ArrayList<Question>();
        questionList = questionMapper.selectByType(type);
        return questionList;
    }

    private List<Question> readBrandPeriodSorXls(InputStream is)
            throws IOException, ParseException {
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        List<Question> questions = new ArrayList<Question>();
        Question question;
        // 循环工作表Sheet
        for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
            HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
            if (hssfSheet == null) {
                continue;
            }
            // 循环行Row，从第二行开始，第一行是字段名称
            for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
                question = new Question();
                HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                for (int i = 0; i < hssfRow.getLastCellNum(); i++) {
                    HSSFCell brandIdHSSFCell = hssfRow.getCell(i);

//                    System.out.println("brandIdHSSFCell = " + brandIdHSSFCell);
                    /*if (i == 0) {
                        question.setQid((int)Float.parseFloat(getCellValue(brandIdHSSFCell)));
                    } else*/
                    if (i == 0) {
                        question.setSubject(getCellValue(brandIdHSSFCell));
                    } else if (i == 1) {
                        question.setA(getCellValue(brandIdHSSFCell));
                    } else if (i == 2) {
                        question.setB(getCellValue(brandIdHSSFCell));
                    } else if (i == 3) {
                        question.setC(getCellValue(brandIdHSSFCell));
                    } else if (i == 4) {
                        question.setD(getCellValue(brandIdHSSFCell));
                    } else if (i == 5) {
                        question.setAnswer(getCellValue(brandIdHSSFCell));
                    } else if (i == 6) {
                        question.setScore((int)Float.parseFloat(getCellValue(brandIdHSSFCell)));
                    } else if (i == 7) {
                        question.setDifficulty((int)Float.parseFloat(getCellValue(brandIdHSSFCell)));
                    } else if (i == 8) {
                        question.setType((int)Float.parseFloat(getCellValue(brandIdHSSFCell)));
                    }
                }
                System.out.println("question = " + question);
                questions.add(question);

            }
        }
        return questions;
    }

   /* _NONE(-1),
    NUMERIC(0),
    STRING(1),
    FORMULA(2),
    BLANK(3),
    BOOLEAN(4),
    ERROR(5);*/
    private String getCellValue(HSSFCell brandIdHSSFCell) {
        switch (brandIdHSSFCell.getCellTypeEnum()) {
            case NUMERIC:
                return brandIdHSSFCell.getNumericCellValue()+"";
            case STRING:
                return brandIdHSSFCell.getStringCellValue();
            case BLANK:
                return "";
            /*case 4:
                if(HSSFDateUtil.isCellDateFormatted(brandIdHSSFCell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", LocaleUtil.getUserLocale());
                    sdf.setTimeZone(LocaleUtil.getUserTimeZone());
                    return sdf.format(brandIdHSSFCell.getDateCellValue());
                }
                return String.valueOf(brandIdHSSFCell.getNumericCellValue());*/
            case BOOLEAN:
                return brandIdHSSFCell.getBooleanCellValue()?"TRUE":"FALSE";
//            case ERROR:
//                return ErrorEval.getText(((BoolErrRecord)brandIdHSSFCell._record).getErrorValue());
            default:
                return "Unknown Cell Type: " + brandIdHSSFCell.getCellTypeEnum();
        }
    }

    public static void main(String[] args) {
        String s = "2.0";
        System.out.println("s = " + Float.parseFloat(s));
    }
}
