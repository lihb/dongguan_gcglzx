package cn.springmvc.service.impl;

import cn.springmvc.dao.UserMapper;
import cn.springmvc.entity.User;
import cn.springmvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMapper userMapper;

	public int insertUser(User user) {
		// TODO Auto-generated method stub
		return userMapper.insert(user);
	}

	public User queryUserById(Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}

	public User queryUserByAccount(String account) {
		return userMapper.queryUserByAccount(account);
	}

	public List<User> queryAllUser() {
		return null;
	}

//	public User queryUserByAccount(String account) {
//		return userMapper.selectUserByAccount(account);
//	}
//
//	public List<User> queryAllUser() {
//		return userMapper.queryAllUser();
//	}

}
