package cn.springmvc.service.impl;

import cn.springmvc.dao.UserCourseMapper;
import cn.springmvc.entity.UserCourse;
import cn.springmvc.service.UserCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/4/13.
 */
@Service
public class UserCourseServiceImpl implements UserCourseService {

    @Autowired
    private UserCourseMapper userCourseMapper;

    public List<UserCourse> getUserCourseByUserId(int userId) {
        return userCourseMapper.getUserCourseByUserId(userId);
    }

    public List<UserCourse> getUserCourseByCourseId(int courseId) {
        return userCourseMapper.getUserCourseByCourseId(courseId);

    }

    public int insert(UserCourse record) {
        return userCourseMapper.insert(record);
    }
}
