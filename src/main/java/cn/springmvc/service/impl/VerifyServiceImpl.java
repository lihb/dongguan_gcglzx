package cn.springmvc.service.impl;

import cn.springmvc.common.ApiCommon;
import cn.springmvc.service.VerifyService;
import cn.springmvc.util.HMACSHA1;
import cn.springmvc.util.JedisClient;
import com.alibaba.druid.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("verifyService")
public class VerifyServiceImpl implements VerifyService {
	
	@Autowired
	private ApiCommon apiCommon;

	public boolean verifyAccount(String sessionKey, String operate,
			String requestURI, String date, int userId, String signature) {
		boolean flag = true;
		StringBuilder sb = new StringBuilder();
		Jedis jedis = JedisClient.getJedis();
		sb.append("SessionKey=" + sessionKey).append("&Operate=" + operate)
				.append("&RequestURI=" + requestURI).append("&Date=" + date);
		System.out.println("参数---"+ sb.toString());
		String sessionSecret=null;
		if (!jedis.exists(String.valueOf(userId))) {
			flag = false;
			System.out.println("sessionkey 已经失效！！");
			return flag;
		}
		sessionSecret = jedis.hget(String.valueOf(userId), "sessionSecret");
		System.out.println("参数sessionSecret---"+ sessionSecret);
		
		//刷新session的有效时间
		jedis.expire(String.valueOf(userId), apiCommon.expireTime); 
		
		String signature2="";
		try {
			signature2 = HMACSHA1.getSignature(sb.toString(),
					sessionSecret);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(!StringUtils.equals(signature2, signature)){
			flag = false;
			System.out.println("signature 不匹配！！");
		}

		return flag;
	}

	public boolean isPhoneNum(String account) {
		// TODO Auto-generated method stub
		/**
		 * 匹配130-139号段，147，150-153，154-159号段，180-189号段
		 */
//		Pattern p = Pattern.compile("^((13[0-9])|(147)|(15[\\d&&[^4]])|(18[0-9]))\\d{8}$");  //电信手机号
		String regex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\\d{8}$"; // 全部手机号
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(account);
		return m.matches();
	}
	
	public static void main(String[] args) {
		String phone = "14980252021";
		VerifyService verifyService = new VerifyServiceImpl();
		System.out.println(verifyService.isPhoneNum(phone));
	}

	
	
	

}
