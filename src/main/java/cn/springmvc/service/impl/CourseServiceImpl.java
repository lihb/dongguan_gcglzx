package cn.springmvc.service.impl;

import cn.springmvc.dao.CourseMapper;
import cn.springmvc.entity.Course;
import cn.springmvc.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lhb on 2017/4/10.
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseMapper courseMapper;

    public int insertCourse(Course course) {
        return courseMapper.insert(course);
    }

    public Course queryCourseById(int id) {
        return courseMapper.selectByPrimaryKey(id);
    }

    public List<Course> queryCourseByWareId(int id) {
//        return courseMapper.queryCourseByWareId(id);
        return null;
    }
}
