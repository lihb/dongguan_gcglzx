package cn.springmvc.service;

import cn.springmvc.entity.User;

import java.util.List;


public interface UserService {

	/**
	 * 插入用户数据
	 * @param user
	 * @return
	 */
	public int insertUser(User user);

	/**
	 * 根据用户id查询数据库
	 * @param id
	 * @return
	 */
	public User queryUserById(Integer id);

	public User queryUserByAccount(String account);

	List<User> queryAllUser();
}
