package cn.springmvc.service;

import cn.springmvc.entity.Question;

import java.io.InputStream;
import java.util.List;

/**
 * Created by lhb on 2017/4/14.
 */
public interface QuestionService {

    void insertData(InputStream is);

    List<Question> selectData(int type);

}
