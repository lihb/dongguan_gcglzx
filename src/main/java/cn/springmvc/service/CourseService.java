package cn.springmvc.service;

import cn.springmvc.entity.Course;

import java.util.List;

/**
 * Created by lhb on 2017/4/10.
 */
public interface CourseService {

    /**
     * 插入课程数据
     * @param course
     * @return
     */
    public int insertCourse(Course course);

    /**
     * 根据课程id查询数据库
     * @param id
     * @return
     */
    public Course queryCourseById(int id);

    public List<Course> queryCourseByWareId(int id);
}
