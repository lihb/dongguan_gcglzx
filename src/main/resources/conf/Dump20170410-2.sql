-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: exam_db
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_course`
--

DROP TABLE IF EXISTS `tb_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course` (
  `course_id` int(11) NOT NULL,
  `course_type` varchar(45) DEFAULT NULL COMMENT '课程类别',
  `course_attachment` varchar(45) DEFAULT NULL COMMENT '附件',
  `course_ware_id` int(11) DEFAULT NULL COMMENT '课程的课件  ',
  `course_score_pass` int(11) DEFAULT NULL COMMENT '多少分及格',
  `course_is_elective` tinyint(1) DEFAULT NULL COMMENT '是否选修',
  `course_tag` varchar(45) DEFAULT NULL COMMENT '课程标签',
  `course_time` varchar(45) DEFAULT NULL COMMENT '课程时长',
  `course_credit` varchar(45) DEFAULT NULL COMMENT '课程学分',
  `course_description` text COMMENT '课程描述',
  `course_cover` varchar(128) DEFAULT NULL COMMENT '课程封面',
  PRIMARY KEY (`course_id`),
  UNIQUE KEY `id_UNIQUE` (`course_id`),
  KEY `course_ware_id_idx` (`course_ware_id`),
  CONSTRAINT `course_ware_id` FOREIGN KEY (`course_ware_id`) REFERENCES `tb_course_ware` (`courseware_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_course`
--

LOCK TABLES `tb_course` WRITE;
/*!40000 ALTER TABLE `tb_course` DISABLE KEYS */;
INSERT INTO `tb_course` VALUES (1,'工程','http://attach.b.com',1,60,0,'移动','45','5','课程描述','http://cover.b.com'),(2,'方法','http://attach.b.com',1,68,1,'联通','40','4','课程描述2','http://cover.b.com'),(3,'方法3','http://attach.b.com',2,68,1,'电信','40','4','课程描述3','http://cover.b.com'),(4,'方法4','http://attach.b.com',3,68,1,'联通','40','4','课程描述4','http://cover.b.com'),(5,'方法5','http://attach.b.com',3,68,1,'电信','40','4','课程描述5','http://cover.b.com'),(6,'方6','http://attach.b.com',5,68,1,'移动','40','4','课程描述6','http://cover.b.com');
/*!40000 ALTER TABLE `tb_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_course_ware`
--

DROP TABLE IF EXISTS `tb_course_ware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course_ware` (
  `courseware_id` int(11) NOT NULL AUTO_INCREMENT,
  `courseware_name` varchar(45) DEFAULT NULL COMMENT '课件名称\n',
  `courseware_status` varchar(45) DEFAULT NULL COMMENT '课件状态',
  `courseware_origin` varchar(45) DEFAULT NULL COMMENT '课件来源',
  `courseware_time` varchar(45) DEFAULT NULL COMMENT '课件学时',
  `courseware_credit` int(11) DEFAULT NULL COMMENT '课件学分',
  `courseware_handout` varchar(45) DEFAULT NULL COMMENT '课件讲义',
  `courseware_category` int(11) DEFAULT NULL COMMENT '课件分组',
  PRIMARY KEY (`courseware_id`),
  UNIQUE KEY `courseware_id_UNIQUE` (`courseware_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_course_ware`
--

LOCK TABLES `tb_course_ware` WRITE;
/*!40000 ALTER TABLE `tb_course_ware` DISABLE KEYS */;
INSERT INTO `tb_course_ware` VALUES (1,'物理','完毕','土豆','50',5,'订单',2),(2,'化学','进行中','优酷','50',5,'订单',1),(3,'化学3','进行中3','优酷3','503',53,'订单3',3),(4,'化学4','进行中4','优酷4','504',54,'订单4',4),(5,'化学5','进行中5','优酷5','505',55,'订单5',5);
/*!40000 ALTER TABLE `tb_course_ware` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_exam`
--

DROP TABLE IF EXISTS `tb_exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_exam` (
  `exam_id` int(11) NOT NULL COMMENT '考试编号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `course_id` int(11) DEFAULT NULL COMMENT '课程id',
  `user_score` int(11) DEFAULT NULL COMMENT '用户考试分数',
  `is_pass` tinyint(1) DEFAULT NULL COMMENT '是否及格，1-及格， 0-不及格',
  `rank` int(11) DEFAULT NULL COMMENT '用户排名',
  `user_count` int(11) DEFAULT NULL COMMENT '考试总人数',
  `score_to_pass` int(11) DEFAULT NULL COMMENT '该课程要及格，所需最低分数',
  PRIMARY KEY (`exam_id`),
  UNIQUE KEY `exam_id_UNIQUE` (`exam_id`),
  KEY `user_id_idx` (`user_id`),
  KEY `course_id_idx` (`course_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_exam`
--

LOCK TABLES `tb_exam` WRITE;
/*!40000 ALTER TABLE `tb_exam` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_exam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_question`
--

DROP TABLE IF EXISTS `tb_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_question` (
  `question_id` int(11) NOT NULL,
  `exam_id` int(11) DEFAULT NULL COMMENT '所属试卷',
  `answer` text COMMENT '答案',
  `a` text COMMENT '选项a',
  `b` text COMMENT '选项b',
  `c` text COMMENT '选项c',
  `d` text COMMENT '选项d',
  `subject` text COMMENT '题目',
  `score` int(11) DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`question_id`),
  KEY `exam_id_idx` (`exam_id`),
  CONSTRAINT `exam_id` FOREIGN KEY (`exam_id`) REFERENCES `tb_exam` (`exam_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='试题表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_question`
--

LOCK TABLES `tb_question` WRITE;
/*!40000 ALTER TABLE `tb_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(45) DEFAULT NULL COMMENT '账户',
  `password` varchar(45) DEFAULT NULL COMMENT '密码',
  `role` int(11) DEFAULT '1' COMMENT '权限',
  `nickname` varchar(30) DEFAULT NULL COMMENT '昵称',
  `self_intro` varchar(200) DEFAULT NULL COMMENT '自我介绍',
  `avatar` varchar(45) DEFAULT NULL COMMENT '封面',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` VALUES (1,'xxxx','password',0,'小孫','自我介紹','http://xx.yy.xom','2017-03-25'),(2,'xxxx','password',0,'小2','自我介紹','http://xx.yy.xom','2017-03-25'),(3,'xxxx','password',0,'小3','自我介紹','http://xx.yy.xom','2017-03-25'),(4,'15820218025','lihb',1,'小3','自我介紹','http://xx.yy.xom','2017-03-25');
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'exam_db'
--

--
-- Dumping routines for database 'exam_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-10 19:36:27
