CREATE DATABASE  IF NOT EXISTS `exam_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `exam_db`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: exam_db
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_course` 1.课程表
--

DROP TABLE IF EXISTS `tb_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course` (
  `cid` int(11) NOT NULL,
  `cname` varchar(256) DEFAULT NULL ,
  `ctype` varchar(45) DEFAULT NULL ,
  `cattach` varchar(45) DEFAULT NULL ,
  `cpasscore` int(11) DEFAULT NULL ,
  `celective` tinyint(1) DEFAULT NULL,
  `cpermission` int(11) DEFAULT NULL,
  `ctag` int(11)  DEFAULT NULL,
  `ctime` int(11)  DEFAULT NULL,
  `ccredit` varchar(45) DEFAULT NULL,
  `cdesc` text ,
  `ccover` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`cid`),
  UNIQUE KEY `id_UNIQUE` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_course`
--

LOCK TABLES `tb_course` WRITE;
/*!40000 ALTER TABLE `tb_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_ware` 2.课件表
--

DROP TABLE IF EXISTS `tb_ware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_ware` (
  `wid` int(11) NOT NULL AUTO_INCREMENT,
  `wname` varchar(45) DEFAULT NULL ,
  `wstatus` varchar(45) DEFAULT NULL ,
  `worigin` varchar(45) DEFAULT NULL ,
  `wtime` int(11) DEFAULT NULL ,
  `wcredit` int(11) DEFAULT NULL ,
  `whandout` varchar(45) DEFAULT NULL ,
  `wcategory` int(11) DEFAULT NULL,
  PRIMARY KEY (`wid`),
  UNIQUE KEY `wid_UNIQUE` (`wid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_ware`
--

LOCK TABLES `tb_ware` WRITE;
/*!40000 ALTER TABLE `tb_ware` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_ware` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_paper` 3.试卷表
--

DROP TABLE IF EXISTS `tb_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_paper` (
  `pid` int(11) NOT NULL,
  `pname` int(11) DEFAULT NULL,
  `passscore` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  UNIQUE KEY `pid_UNIQUE` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_paper`
--

LOCK TABLES `tb_paper` WRITE;
/*!40000 ALTER TABLE `tb_paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_question` 4.题库表
--

DROP TABLE IF EXISTS `tb_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_question` (
  `qid` int(11) NOT NULL,
  `subject` text ,
  `a` text  ,
  `b` text  ,
  `c` text  ,
  `d` text  ,
  `answer` text,
  `score` int(11) DEFAULT NULL ,
  `difficulty` int(11) DEFAULT 1,
  `type` int(11) DEFAULT 1,
  PRIMARY KEY (`qid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_question`
--

LOCK TABLES `tb_question` WRITE;
/*!40000 ALTER TABLE `tb_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user` 5.用户表
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL ,
  `password` varchar(45) DEFAULT NULL ,
  `email` varchar(45) DEFAULT NULL ,
  `nickname` varchar(30) DEFAULT NULL ,
  `intro` varchar(200) DEFAULT NULL ,
  `avatar` varchar(45) DEFAULT NULL ,
  `role` int(11) DEFAULT '1' ,
  `create_time` date DEFAULT NULL ,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid_UNIQUE` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `tb_user_paper`  6.用户试卷关系表
--

DROP TABLE IF EXISTS `tb_user_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user_paper` (
  `upid`  int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT 0,
  `pass` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`upid`),
  UNIQUE KEY `upid_UNIQUE` (`upid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user_paper`
--

LOCK TABLES `tb_user_paper` WRITE;
/*!40000 ALTER TABLE `tb_user_paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_user_paper` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `tb_user_course` 7.用户课程关系表
--

DROP TABLE IF EXISTS `tb_user_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user_course` (
  `ucid`  int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ucid`),
  UNIQUE KEY `ucid_UNIQUE` (`ucid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user_course`
--

LOCK TABLES `tb_user_course` WRITE;
/*!40000 ALTER TABLE `tb_user_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_user_course` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `tb_course_ware` 8.课程课件关系表
--

DROP TABLE IF EXISTS `tb_course_ware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course_ware` (
  `cwid`  int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `wid` int(11) DEFAULT NULL,
  PRIMARY KEY (`cwid`),
  UNIQUE KEY `cwid_UNIQUE` (`cwid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_course_ware`
--

LOCK TABLES `tb_course_ware` WRITE;
/*!40000 ALTER TABLE `tb_course_ware` DISABLE KEYS */ ;
/*!40000 ALTER TABLE `tb_course_ware` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_course_paper` 9.课程试卷关系表
--

DROP TABLE IF EXISTS `tb_course_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course_paper` (
  `cpid`  int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`cpid`),
  UNIQUE KEY `cpid_UNIQUE` (`cpid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_course_paper`
--

LOCK TABLES `tb_course_paper` WRITE;
/*!40000 ALTER TABLE `tb_course_paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_course_paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_paper_question` 10.试卷题库关系表
--

DROP TABLE IF EXISTS `tb_paper_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_paper_question` (
  `pqid`  int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `qid` int(11) DEFAULT NULL,
  PRIMARY KEY (`pqid`),
  UNIQUE KEY `pqid_UNIQUE` (`pqid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_paper_question`
--

LOCK TABLES `tb_paper_question` WRITE;
/*!40000 ALTER TABLE `tb_paper_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_paper_question` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `tb_achieve` 11.成绩表
--

DROP TABLE IF EXISTS `tb_achieve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_achieve` (
  `aid`  int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid_UNIQUE` (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_achieve`
--

LOCK TABLES `tb_achieve` WRITE;
/*!40000 ALTER TABLE `tb_achieve` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_achieve` ENABLE KEYS */;
UNLOCK TABLES;




/*--
-- Table structure for table `tb_user_ware` 用户课件关系表
--

DROP TABLE IF EXISTS `tb_user_ware`;
*//*!40101 SET @saved_cs_client     = @@character_set_client *//*;
*//*!40101 SET character_set_client = utf8 *//*;
CREATE TABLE `tb_user_ware` (
  `uwid`  int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`upid`),
  UNIQUE KEY `upid_UNIQUE` (`upid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
*//*!40101 SET character_set_client = @saved_cs_client *//*;

--
-- Dumping data for table `tb_user_ware`
--

LOCK TABLES `tb_user_ware` WRITE;
!40000 ALTER TABLE `tb_user_ware` DISABLE KEYS ;
!40000 ALTER TABLE `tb_user_ware` ENABLE KEYS ;
UNLOCK TABLES;*/

--
-- Dumping events for database 'exam_db'
--

--
-- Dumping routines for database 'exam_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-10 19:36:27
