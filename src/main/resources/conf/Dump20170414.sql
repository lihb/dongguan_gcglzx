CREATE DATABASE  IF NOT EXISTS `exam_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `exam_db`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: exam_db
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_achieve`
--

DROP TABLE IF EXISTS `tb_achieve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_achieve` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid_UNIQUE` (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_achieve`
--

LOCK TABLES `tb_achieve` WRITE;
/*!40000 ALTER TABLE `tb_achieve` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_achieve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_course`
--

DROP TABLE IF EXISTS `tb_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course` (
  `cid` int(11) NOT NULL,
  `cname` varchar(256) DEFAULT NULL,
  `ctype` varchar(45) DEFAULT NULL,
  `cattach` varchar(45) DEFAULT NULL,
  `cpasscore` int(11) DEFAULT NULL,
  `celective` tinyint(1) DEFAULT NULL,
  `cpermission` int(11) DEFAULT NULL,
  `ctag` int(11) DEFAULT NULL,
  `ctime` int(11) DEFAULT NULL,
  `ccredit` varchar(45) DEFAULT NULL,
  `cdesc` text,
  `ccover` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`cid`),
  UNIQUE KEY `id_UNIQUE` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_course`
--

LOCK TABLES `tb_course` WRITE;
/*!40000 ALTER TABLE `tb_course` DISABLE KEYS */;
INSERT INTO `tb_course` VALUES (1,'化学','1','附件_化学',80,1,0,1,20,'3','课程描述1','http://cover1.com'),(2,'数学','1','附件_数学',70,1,1,1,22,'2','课程描述2','http://cover2.com'),(3,'物理','1','附件_物理',70,1,1,0,23,'4','课程描述3','http://cover3.com'),(4,'语文','1','附件_语文',80,1,1,1,25,'5','课程描述4','http://cover4.com'),(5,'英语','1','附件_英语',50,1,1,0,29,'3','课程描述5','http://cover5.com');
/*!40000 ALTER TABLE `tb_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_course_paper`
--

DROP TABLE IF EXISTS `tb_course_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course_paper` (
  `cpid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`cpid`),
  UNIQUE KEY `cpid_UNIQUE` (`cpid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_course_paper`
--

LOCK TABLES `tb_course_paper` WRITE;
/*!40000 ALTER TABLE `tb_course_paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_course_paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_course_ware`
--

DROP TABLE IF EXISTS `tb_course_ware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_course_ware` (
  `cwid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `wid` int(11) DEFAULT NULL,
  PRIMARY KEY (`cwid`),
  UNIQUE KEY `cwid_UNIQUE` (`cwid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_course_ware`
--

LOCK TABLES `tb_course_ware` WRITE;
/*!40000 ALTER TABLE `tb_course_ware` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_course_ware` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_paper`
--

DROP TABLE IF EXISTS `tb_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_paper` (
  `pid` int(11) NOT NULL,
  `pname` int(11) DEFAULT NULL,
  `passscore` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  UNIQUE KEY `pid_UNIQUE` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_paper`
--

LOCK TABLES `tb_paper` WRITE;
/*!40000 ALTER TABLE `tb_paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_paper_question`
--

DROP TABLE IF EXISTS `tb_paper_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_paper_question` (
  `pqid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `qid` int(11) DEFAULT NULL,
  PRIMARY KEY (`pqid`),
  UNIQUE KEY `pqid_UNIQUE` (`pqid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_paper_question`
--

LOCK TABLES `tb_paper_question` WRITE;
/*!40000 ALTER TABLE `tb_paper_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_paper_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_question`
--

DROP TABLE IF EXISTS `tb_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_question` (
  `qid` int(11) NOT NULL,
  `subject` text,
  `a` text,
  `b` text,
  `c` text,
  `d` text,
  `answer` text,
  `score` int(11) DEFAULT NULL,
  `difficulty` int(11) DEFAULT '1',
  `type` int(11) DEFAULT '1',
  PRIMARY KEY (`qid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_question`
--

LOCK TABLES `tb_question` WRITE;
/*!40000 ALTER TABLE `tb_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `nickname` varchar(30) DEFAULT NULL,
  `intro` varchar(200) DEFAULT NULL,
  `avatar` varchar(45) DEFAULT NULL,
  `role` int(11) DEFAULT '1',
  `create_time` date DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid_UNIQUE` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` VALUES (1,'lihb','123','lihb@emali.com','lhb','自我介绍1','http://avater1.com',1,'2017-04-30'),(2,'lihb2','123','lihb2@emali.com','lhb2','自我介绍2','http://avater2.com',0,'2017-04-30'),(3,'lihb3','123','lihb3@emali.com','lhb3','自我介绍3','http://avater3.com',1,'2017-04-30'),(4,'lihb4','1234','lihb4@emali.com','lhb4','自我介绍3','http://avater4.com',1,'2017-04-30'),(5,'lihb5','12345','lihb5@emali.com','lhb5','自我介绍4','http://avater5.com',1,'2017-04-30');
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user_course`
--

DROP TABLE IF EXISTS `tb_user_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user_course` (
  `ucid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ucid`),
  UNIQUE KEY `ucid_UNIQUE` (`ucid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user_course`
--

LOCK TABLES `tb_user_course` WRITE;
/*!40000 ALTER TABLE `tb_user_course` DISABLE KEYS */;
INSERT INTO `tb_user_course` VALUES (1,1,1,0),(2,1,2,1),(3,3,3,1),(4,3,2,0),(5,4,2,1),(6,5,5,0),(7,2,3,0),(8,4,1,0),(9,4,1,2);
/*!40000 ALTER TABLE `tb_user_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user_paper`
--

DROP TABLE IF EXISTS `tb_user_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user_paper` (
  `upid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT '0',
  `pass` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`upid`),
  UNIQUE KEY `upid_UNIQUE` (`upid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user_paper`
--

LOCK TABLES `tb_user_paper` WRITE;
/*!40000 ALTER TABLE `tb_user_paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_user_paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_ware`
--

DROP TABLE IF EXISTS `tb_ware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_ware` (
  `wid` int(11) NOT NULL AUTO_INCREMENT,
  `wname` varchar(45) DEFAULT NULL,
  `wstatus` varchar(45) DEFAULT NULL,
  `worigin` varchar(45) DEFAULT NULL,
  `wtime` int(11) DEFAULT NULL,
  `wcredit` int(11) DEFAULT NULL,
  `whandout` varchar(45) DEFAULT NULL,
  `wcategory` int(11) DEFAULT NULL,
  PRIMARY KEY (`wid`),
  UNIQUE KEY `wid_UNIQUE` (`wid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_ware`
--

LOCK TABLES `tb_ware` WRITE;
/*!40000 ALTER TABLE `tb_ware` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_ware` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-14  0:21:59
